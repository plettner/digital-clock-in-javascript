class DigitalClock {
  
  constructor(canvasId = "digital-clock-item-canvas", properties = {}) {
    // Get the canvas and the context for the canvas
    this.canvas = document.getElementById(canvasId);
    this.context = this.canvas.getContext("2d");

    this.width = properties.width ? properties.width : this.canvas.width;
    this.height = properties.height ? properties.height : this.canvas.height;
    this.bgColor = properties.bgColor ? properties.bgColor : "#000000";
    this.fgColor = properties.fgColor ? properties.fgColor : "#00ff00";
  }

  static instantiate(canvasId = 'digital-clock-item-canvas', properties = {}) {
    return new DigitalClock(canvasId, properties);
  }

  /**
   * Draws a digital clock.
   * @param {*} x -- horizontal location to start drawing at
   * @param {*} y -- vertical location to start drawing at
   */
  draw(x, y) {
    var digitCount = 6;
    var ssdWidth = this.width / 7;
    var ssdHeight = this.height;
    var gapWidth = ssdWidth / 19;

    var valueArray = this.getTimeArray();

    var props = this.getProperties(ssdWidth, ssdHeight);

    var ssd = new SevenSegmentDisplay(this.context, props);
    for (var index = 0; index < digitCount; index++) {
      ssd.draw(x, y, valueArray[index]);

      if ((index & 0x01) === 0) {
        x += ssdWidth + gapWidth;
      } else {
        x += ssdWidth + gapWidth * 8;
      }
    }

    // Now draw the separators between hours and minutes
    var h = ssdHeight / 4;
    var w = gapWidth * 4;

    var separator = new SSDSeparator(
      this.context,
      w,
      h,
      this.bgColor,
      this.fgColor
    );
    x = ssdWidth * 2 + gapWidth * 3;
    y = ssdHeight / 2 - ssdHeight / 8;
    separator.draw(x, y);

    // And draw the separators between minutes and seconds
    x = ssdWidth * 4 + gapWidth * 8 + gapWidth * 4;
    // the y parameter is the same as the other separator
    separator.draw(x, y);
  }

  /**
   * Repeatedly draws the digital clock at x,y and with a refresh of delay
   * @param {*} params -- contains the values x,y and delay.  Delay is the number of milliseconds between redraws.
   */
  drawRepeatedly(params) {
    this.draw(params.x, params.y);

    // some interesting magic to invoke a class' method in setTimeout
    // setTimeout(this.drawRepeatedly.bind(this), params.delay, params);
    setTimeout(() => {
      this.drawRepeatedly(params);
    }, params.delay);
  };

  createValueArray(value, digitCount) {
    var arr = [];

    for (var index = 0; index < digitCount; index++) {
      arr.unshift(value % 10);
      value = Math.floor(value / 10);
    }

    return arr;
  };

  // Returns an array of six digits representing the
  // current time as HHMMSS
  getTimeArray() {
    var arr = [];
    var date = new Date();

    arr.push(Math.floor(date.getHours() / 10));
    arr.push(date.getHours() % 10);

    arr.push(Math.floor(date.getMinutes() / 10));
    arr.push(date.getMinutes() % 10);

    arr.push(Math.floor(date.getSeconds() / 10));
    arr.push(date.getSeconds() % 10);

    return arr;
  };

  getProperties(width, height) {
    return {
      width: width,
      height: height,
      bgColor: this.bgColor,
      fgColor: this.fgColor,
    };
  }
}
/**
 * Encapsulates drawing methods for a Seven Segment "LED" Display.
 * @param {*} context the drawing context for a canvas
 * @param {*} props properties (colors, width, height) of this SSD
 */
class SevenSegmentDisplay {
  
  constructor(context, props) {
    this.segmentCount = 8; // seven segments and a dot
    this.context = context;
    this.width = props.width;
    this.height = props.height;
    this.bgcolor = props.bgColor;
    this.fgColor = props.fgColor;
  }

  draw(x, y, value, decimalPoint) {
    // True if the decimal point should be illuminated
    this.decimalPointOn = decimalPoint;

    this.context.save();

    // First, draw the background.
    this.context.fillStyle = this.bgColor;
    this.context.strokeStyle = this.bgColor;
    this.context.fillRect(x, y, this.width, this.height);

    // We set the segment color here rather than reset it each
    // time for each segment.
    this.context.fillStyle = this.fgColor;
    this.context.strokeStyle = this.fgColor;
    for (var index = 0; index < this.segmentCount; index++) {
      if (this.isSegmentOn(value, index)) {
        this.drawSegment(x, y, index);
      }
    }

    this.context.restore();
  };

  /**
   * Draws a single segment of the seven segment digit.
   * @param {*} x -- horizontal location to start drawing
   * @param {*} y -- vertical location to start drawing
   * @param {*} segmentIndex -- index of the segment being drawn.
   * 0 is the top "bar", then count clockwise around, and finish
   * with 6 being the "crossbar".  Decimal point is 7.
   */
  drawSegment(x, y, segmentIndex) {
    this.context.save();
    this.context.translate(x, y);

    var hseg = this.width / 12;
    var vseg = this.height / 25;

    var hthick = hseg;
    var vthick = vseg;

    var hwidth = hseg * 7;
    var vheight = vseg * 10;

    var xx, yy, hh, ww;

    switch (segmentIndex) {
      case 0:
        xx = hseg * 2;
        yy = vseg;
        ww = hwidth;
        hh = vthick;
        break;

      case 1:
        xx = hseg * 9;
        yy = vseg * 2;
        ww = hseg;
        hh = vheight;
        break;

      case 2:
        xx = hseg * 9;
        yy = vseg * 13;
        ww = hseg;
        hh = vheight;
        break;

      case 3:
        xx = hseg * 2;
        yy = vseg * 23;
        ww = hwidth;
        hh = vthick;
        break;

      case 4:
        xx = hseg;
        yy = vseg * 13;
        ww = hseg;
        hh = vheight;
        break;

      case 5:
        xx = hseg;
        yy = vseg * 2;
        ww = hseg;
        hh = vheight;
        break;

      case 6:
        xx = hseg * 2;
        yy = vseg * 12;
        ww = hwidth;
        hh = vthick;
        break;

      case 7:
        xx = hseg * 10;
        yy = vseg * 23;
        ww = hseg;
        hh = vseg;
        break;
    }

    this.context.fillRect(xx, yy, ww, hh);

    this.context.restore();
  };

  isSegmentOn(value, segmentIndex) {
    switch (segmentIndex) {
      case 0:
        // On for 0, 2, 3, 5, 7, 8, 9
        return !(value === 1 || value === 4 || value === 6);
      case 1:
        // 0, 1, 2, 3, 4, 7, 8, 9
        return !(value === 5 || value === 6);
      case 2:
        // On for 0, 1, 3, 4, 5, 6, 7, 8, 9
        return !(value === 2);
      case 3:
        // On for 0, 2, 3, 5, 6, 8
        return !(value === 1 || value === 4 || value === 7 || value === 9);
      case 4:
        // On for 0, 2, 6, 8
        return value === 0 || value === 2 || value === 6 || value === 8;
      case 5:
        // On for 0, 4, 5, 6, 8, 9
        return !(value === 1 || value === 2 || value === 3 || value === 7);
      case 6:
        // On for 2, 3, 4, 5, 6, 8, 9
        return !(value === 0 || value === 1 || value === 7);
      case 7:
        return this.decimalPointOn;
    }
  }
}

/**
 * Encapsulates drawing routines for Seven Segment Display separators
 * @param {*} context canvas context for drawing
 * @param {*} props properties such as dimensions and colors
 */
class SSDSeparator {

  constructor(context, width, height, bgColor, fgColor) {
    this.context = context;
    this.width = width;
    this.height = height;
    this.bgColor = bgColor;
    this.fgColor = fgColor;
    this.segWidth = this.width / 4;
    this.segHeight = this.height / 8;
  }

  draw(x, y) {
    this.context.save();
    this.context.translate(x, y);

    this.context.fillStyle = this.bgColor;
    this.context.strokeStyle = this.bgColor;
    this.context.fillRect(0, 0, this.width, this.height);

    this.drawTopDot(x, y);
    this.drawBottomDot(x, y);

    this.context.restore();
  };

  drawTopDot(x, y) {
    this.context.fillStyle = this.fgColor;
    this.context.strokeStyle = this.fgColor;
    var xx = this.segWidth;
    var yy = this.segHeight;
    var ww = this.segWidth * 2;
    var hh = this.segHeight * 2;
    this.context.fillRect(xx, yy, ww, hh);
  };

  drawBottomDot(x, y) {
    this.context.fillStyle = this.fgColor;
    this.context.strokeStyle = this.fgColor;
    var xx = this.segWidth;
    var yy = this.segHeight * 5;
    var ww = this.segWidth * 2;
    var hh = this.segHeight * 2;
    this.context.fillRect(xx, yy, ww, hh);
  }

}
