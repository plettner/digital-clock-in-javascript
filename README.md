# Digital Clock in Javascript

Wrote a simple digital clock in Javascript using the Canvas feature.  It's not the sexiest looking clock but this was more about coding in Javascript than it was creating something truly presentable.

## Usage

```
    <script src="digitalclock.js"></script>
    <script>
        window.onload=function() {
            digitalClock = new DigitalClock("myCanvas",
                {
                  // Override the canvas settings here
                  width:200, height:50,
                  fgColor:'#00ff00', bgColor:'black', 
                }
            );

            // Draw the clock repeatedly, refreshing every 500ms
            digitalClock.drawRepeatedly({x:0, y:0, delay:500});
        };
    </script>


    ...

    <canvas id="myCanvas" width="400" height="100">
        Your browser does not support the canvas element.
    </canvas>


```

## Screenshot

Here's what the running code should look like:

![Digital Clock Example](./screenshot.png)

Invoke the method drawDigitalCLock() and pass the following values:

* the ID of the canvas HTML tag (in our case, it was `myCanvas`)
* an object containing any or all of the following values
    * *width* -- width in pixels of the entire display (defaults to canvas width)
    * *height* -- height in pixels of the entire display (defaults to canvas height)
    * *fgColor* -- color of the "LED" segments (defaults to 0x00ff00, bright green)
    * *bgcolor* -- background color of each seven segment display unit (defaults to black)

Note:  For the moment at least, it's a 24 hour clock.

## Known Issues / Potential Improvements

* Resizing works, but it's dependent on the size of the canvas being set explicitly.
  Setting the canvas width as percentages (i.e., `width : 100%`) seems to confound
  the code.
* Javascript doesn't have an event that notifies the code that the canvas or window
  has been resized.  The workaround is to restart everything in the `windows.onresize`
  handler.  It works, but it would be nice not to reload everything.
* I found that the code won't run two clock displays.  I haven't investigated why.
* The clock completely redraws itself every half-second.  Nicer, more elegant code
  would redraw only the changed digits when necessary.  This was an exercise in 
  using Javascript classes, so I haven't pursued a more "proper" animation approach.
